# настройка провайдера
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

# Яндексу нужно явно ууказать, что он яндекс и зону, ЦОДв ресурсы которого мы используем
provider "yandex" {
  zone = "ru-central1-a"
}

# в этом блоке создаются ресурсы
# план ниже описывает облачную сеть network-1 с подсетью subnet-1 в зоне доступности ru-central1-a
# и одну виртуальные машину (2 ядра и 2 ГБ оперативной памяти). 
# Она автоматически получат публичные и внутренние IP-адреса из диапазона 192.168.10.0/24 в подсети subnet-1, 
# а также публичные ключи длоя досутпа с локального ноутбука

resource "yandex_compute_disk" "boot-disk-1" {
  name     = "boot-disk-1"
  type     = "network-hdd"
  zone     = "ru-central1-a"
  size     = "20"
  image_id = var.image_ubuntu
}

resource "yandex_compute_instance" "vm-1" {
  name = "terraform1"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk-1.id
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    user-data = local.user_meta
  }

  provisioner "remote-exec" {
    inline = ["echo 'Hello world!' && sleep 5 && python3 --version && sleep 5"]

    connection {
      host    = self.network_interface.0.nat_ip_address
      type    = "ssh"
      user    = var.user_name
      agent   = true
      timeout = 120
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u ${var.user_name} -i '${self.network_interface.0.nat_ip_address},' --private-key '${file("${var.private_key_path}")}' ../ansible/playbook.yml"
  }
}

resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

