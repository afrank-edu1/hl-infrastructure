locals {
  user_meta        = "#cloud-config\nusers:\n  - name: ${var.user_name}\n    groups: sudo\n    shell: /bin/bash\n    sudo: 'ALL=(ALL) NOPASSWD:ALL'\n    ssh-authorized-keys:\n      - ${file("${var.public_key_path}")}"
}