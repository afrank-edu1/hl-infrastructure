variable "image_centos" {
  description = "centos-7"
  default = "fd8g8ia6cjdkc72ukbh4"
}

variable "image_ubuntu" {
  description = "ubuntu-20-04-lts-v20230626"
  default     = "fd808e721rc1vt7jkd0o"
}

 variable "public_key_path"{
    description = "path to pub key"
    default = "~/.ssh/yc_ed25519.pub"
 }

variable "private_key_path"{
    description = "path to pub key"
    default = "~/.ssh/yc_ed25519"
 }

variable "user_name" {}
